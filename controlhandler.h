#include <iostream>
#include "player.h"
#include <string>

enum Control { up, down, left, right, attack, unkown, over };

void EnterCommand(Player& pl, const Control ctrl, bool& kp);
Control GetCommandType(const std::string c);
bool IsPrizeFound(const int xplayer, const int yplayer, const int xprize, const int yprize);