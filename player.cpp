#include "player.h"

bool Player::MoveUp(void) {
    if( ycurrentpos - world.GetTileSize() >= world.GetBorderTop() ) {
        ycurrentpos--;
        return true;
    } else {
        return false;
    }
}
bool Player::MoveDown(void) {
    if( ycurrentpos + world.GetTileSize() <= world.GetBorderBottom() ) {
        ycurrentpos++;
        return true;
    } else {
        return false;
    }
}
bool Player::MoveLeft(void) {
    if( xcurrentpos - world.GetTileSize() >= world.GetBorderLeft() ) {
        xcurrentpos--;
        return true;
    } else {
        return false;
    }
}
bool Player::MoveRight(void) {
    if( xcurrentpos + world.GetTileSize() <= world.GetBorderRight() ) {
        xcurrentpos++;
        return true;
    } else {
        return false;
    }
}
bool Player::Attack(void) {
    return false;
}

std::string Player::GetCurrentPos(void) {
    return "{x: " + std::to_string(xcurrentpos) + ", y: " + std::to_string(ycurrentpos) + "}";
}