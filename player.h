#ifndef PLAYER_H_
#define PLAYER_H_

#include <iostream>
#include <string>
#include "world.h"

class Player {
    public: 
        Player(World& w):world(w), xcurrentpos(0), ycurrentpos(0) { 
            srand (time(NULL));
            xprizepos = rand() % 9 + 2;
            yprizepos = rand() % 9 + 2;
        }
        ~Player() { std::cout << ">> Player is removed!" << std::endl; }

        bool MoveUp(void);
        bool MoveDown(void);
        bool MoveLeft(void);
        bool MoveRight(void);
        bool Attack(void);

        int GetXCurrentPos(void) { return xcurrentpos; }
        int GetYCurrentPos(void) { return ycurrentpos; }
        int GetXPrizePos(void) { return xprizepos; }
        int GetYPrizePost(void) { return yprizepos; }

        std::string GetCurrentPos(void);
    protected: 
    private: 
        int xcurrentpos;
        int ycurrentpos;
        int xprizepos;
        int yprizepos;
        World& world;

        void SetXCurrentPos(const int xpos) { xcurrentpos = xpos; }
        void SetYCurrentPos(const int ypos) { ycurrentpos = ypos; }
};

#endif // PLAYER_H_