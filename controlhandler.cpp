#include "controlhandler.h"

using namespace std;

void EnterCommand(Player& pl, Control ctrl, bool& kp) {
    string current = "Your current position is: ";
    string cannot = "You cannot move in that direction anymore. ";
    switch(ctrl) {
        case Control::left: 
            if(pl.MoveLeft()) cout << current << pl.GetCurrentPos() << endl;
            else cout << cannot << pl.GetCurrentPos() << endl;

            if(IsPrizeFound(pl.GetXCurrentPos(), pl.GetYCurrentPos(), pl.GetXPrizePos(), pl.GetYPrizePost())) kp = false;

            break;
        case Control::right: 
            if(pl.MoveRight()) cout << current << pl.GetCurrentPos() << endl;
            else cout << cannot << pl.GetCurrentPos() << endl;

            if(IsPrizeFound(pl.GetXCurrentPos(), pl.GetYCurrentPos(), pl.GetXPrizePos(), pl.GetYPrizePost())) kp = false;

            break;
        case Control::up: 
            if(pl.MoveUp()) cout << current << pl.GetCurrentPos() << endl;
            else cout << cannot << pl.GetCurrentPos() << endl;

            if(IsPrizeFound(pl.GetXCurrentPos(), pl.GetYCurrentPos(), pl.GetXPrizePos(), pl.GetYPrizePost())) kp = false;

            break;
        case Control::down:
            if(pl.MoveDown()) cout << current << pl.GetCurrentPos() << endl;
            else cout << cannot << pl.GetCurrentPos() << endl;

            if(IsPrizeFound(pl.GetXCurrentPos(), pl.GetYCurrentPos(), pl.GetXPrizePos(), pl.GetYPrizePost())) kp = false;

            break;
        case Control::over:
            kp = false;
            break;
        case Control::attack:
            cout << "Pew! pew! Die peach!" << endl;
            break;
        default:
            cout << "You can't do that." << endl;

    }
}

Control GetCommandType(const std::string c) {
    if(c == "a" || c == "A") {
        return Control::left;
    } else if(c == "d" || c == "D") {
        return Control::right;
    } else if(c == "w" || c == "W") {
        return Control::up;
    } else if ( c == "s" || c == "S" ) {
        return Control::down;
    } else if (c == "f" || c == "F") {
        return Control::attack;
    } else if (c == "x" || c == "X") {
        return Control::over;
    } else {
        return unkown;
    }
}

bool IsPrizeFound(const int xplayer, const int yplayer, const int xprize, const int yprize) {
    if(xplayer == xprize && yplayer == yprize) {
        cout << "You have found the secret treasure. Gratz!" << endl;
        return true;
    }

    return false;
}