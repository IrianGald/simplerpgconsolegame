//
// abstractgame.cpp
//
// Creado por Irian Gald
//
// Descripción: 
//

#include <iostream>
#include "world.h"
#include "player.h"
#include "controlhandler.h"

using namespace std;

int main() {
    World world(10, 10);
    Player player(world);
    bool keepplaying = true;
    string cmdstr;

    cout << endl << endl;

    while(keepplaying) {

        cout << "What you want to do?" << endl;
        cin >> cmdstr;

        cout << endl << "-----------------------" << endl << endl;

        EnterCommand(player, GetCommandType(cmdstr), keepplaying);

    }

    cout << "Game Over" << endl;

    cout << endl;

    return EXIT_SUCCESS;
}