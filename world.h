#ifndef WORLD_H_
#define WORLD_H_

#include <iostream>

class World {
    public: 
        World(const int br, const int bb):borderright(br), borderbottom(bb) {  }
        ~World() { std::cout << ">> World is removed!" << std::endl; }

        void SetBorderBottom(const int bb) { borderbottom = bb; }
        void SetBorderRight(const int br) { borderright = br; }

        int GetBorderBottom(void) const { return borderbottom; }
        int GetBorderRight(void) const { return borderright; }
        int GetBorderTop(void) const { return kBorderTop; }
        int GetBorderLeft(void) const { return kBorderLeft; }
        int GetTileSize(void) const { return kTileSize; }
    protected: 
    private: 
        int borderbottom;
        int borderright;

        // constants
        static const int kTileSize;
        static const int kBorderTop;
        static const int kBorderLeft;
};

#endif // WORLD_H_